var appCtrl = angular.module('appCtrl', []);
var appServices = angular.module('appServices', []);
var appDirectives = angular.module('appDirectives', []);
var app = angular.module('app', [
    'ngRoute',
    'appCtrl',
    'appServices',
    'appDirectives'
]);

app.run(function () {
    console.log('angular loaded');
});

//@prepros-append controllers/*
//@prepros-append directives/*
//@prepros-append services/*


$(document).ready(function () {

    function slideModule() {

        this.listItemSize = $('#slider>li:first-child').width();
        this.totalLi = $('#slider>li').length;

        this.init = function () {
            var container = $('.gallery-container .main').width();

            // Set the width
            $('#slider').css('width', (this.listItemSize * (this.totalLi + 2)));
            $('#slider>li').each(function (key, val) {
                console.log(key);
                $(val).css({
                    'width': container,
                    "transform": "translate(" + (container * key) + "px,0)",
                    "-webkit-transform": "translate(" + (container * key) + "px,0)"
                })
            });

            // Set the First one active
            $('#slider>li:first-child').addClass('active');


            // Set Prev and Next Button
            $('.gallery-container .nextSlide>button').click(function () {
                var nextSlideIndex = $('#slider li.active').index() + 1;
                var getCurrentTransform = $('#slider li.active').css('transform').split(',');
                getCurrentTransform = parseInt(getCurrentTransform[4]);

                $('#slider li.active').css({
                    "transform": "translate(" + (getCurrentTransform - container) + "px,0)",
                    "-webkit-transform": "translate(" + (getCurrentTransform - container) + "px,0px)"
                }).removeClass('active');

                $('#slider li:nth-child(' + (nextSlideIndex + 1) + ')').css({
                    "transform": "translate(" + (getCurrentTransform - container) + "px,0)",
                    "-webkit-transform": "translate(" + (getCurrentTransform - container) + "px,0px)"
                }).addClass('active');

                if ($('#slider li:last-child').hasClass('active')) {
                    $(this).attr('disabled', '')
                    $('.gallery-container .prevSlide>button').removeAttr('disabled');
                }
            });
            $('.gallery-container .prevSlide>button').click(function () {
                var currentIndex = $('#slider li.active').index();
                var nextSlideIndex = $('#slider li.active').index();
                var getCurrentTransform = $('#slider li.active').css('transform').split(',');
                getCurrentTransform = parseInt(getCurrentTransform[4]);

                $('#slider li.active').css({
                    "transform": "translate(" + (getCurrentTransform + container) + "px,0)",
                    "-webkit-transform": "translate(" + (getCurrentTransform + container) + "px,0px)"
                }).removeClass('active');

                $('#slider li:nth-child(' + nextSlideIndex + ')').css({
                    "transform": "translate(" + (getCurrentTransform + container) + "px,0)",
                    "-webkit-transform": "translate(" + (getCurrentTransform + container) + "px,0px)"
                }).addClass('active');

                if ($('#slider li:first-child').hasClass('active')) {
                    $(this).attr('disabled', '')
                    $('.gallery-container .nextSlide>button').removeAttr('disabled');
                }


            });


        };


        this.init();

    }

    var slider = new slideModule();


});