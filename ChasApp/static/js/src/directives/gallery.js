appDirectives.directive('gallery', function () {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: '/static/partials/gallery.html'
    }
});