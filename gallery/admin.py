from django.contrib import admin


# Register your models here.
from .models import Gallery

# Set Admin fields here
class GalleryAdmin(admin.ModelAdmin):
    list_display = ['title', 'updated']


admin.site.register(Gallery, GalleryAdmin)