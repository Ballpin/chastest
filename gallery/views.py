from django.shortcuts import render, get_object_or_404, get_list_or_404

# Get Models

from .models import Gallery


# Create your views here.
def index(request):
    my_objects = get_list_or_404(Gallery)
    def sortItems():

        # Init Vars
        my_objects_length = len(my_objects)
        list = []

        # Vars to end while loop
        i = 0
        endLoop = my_objects_length

        while (endLoop > 0):

            if not list:
                # List is Empty
                list += [my_objects[0:28]]
            elif endLoop < 0:
                # Add The lst items into last Array
                list += [my_objects[len(list[i]):my_objects_length]]
                break
            else:
                # Keep Adding 28 items into the array
                list += [my_objects[len(list[i]):my_objects_length - len(list)]]
                print(len(list[i]))
                i += 1

            # Step down until we cant fit 28 item in one array
            endLoop -= 28
        print(list)
        return list

    context = {
        'imageList': sortItems(),
        'imageListLength': len(my_objects)
    }
    return render(request, 'index.html', context)
