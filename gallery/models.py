from django.db import models


# Create your models here.
def upload_location(instance, filename):
    return '%s/%s' % (instance.id, filename)


# Create your models here.
class Gallery(models.Model):
    title = models.CharField(max_length=100, blank=False)
    image = models.ImageField(upload_to="gallery", null=True, blank=True)
    updated = models.DateTimeField(auto_now=True, auto_now_add=False)

    def __str__(self):
        return self.title
